<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    public function index(){
        $posts = DB::table('pertanyaan')->get();
        //dd($posts);
    	return view('items.index', compact('posts'));
    }

    public function create(){
    	return view('items.create');
    }

    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'judul' => "required|unique:pertanyaan",
            'isi' => "required"
        ]);

        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi"   => $request["isi"],
            "tanggal_dibuat" => date("Y-m-d"),
            "tanggal_diperbaharui" => date("Y-m-d")
        ]);
        
        return redirect('/pertanyaan')->with('success','Tambah Pertanyaan Berhasil'); 
    }
}
