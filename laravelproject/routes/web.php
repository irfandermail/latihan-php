<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/pertanyaan', 'App\Http\Controllers\HomeController@index');
Route::post('/pertanyaan', 'App\Http\Controllers\HomeController@store');
Route::get('/pertanyaan/create', 'App\Http\Controllers\HomeController@create');


Route::get('/', function () {
    //return view('items.index');
    //return view('welcome');
});




/*
Route::get('/pertanyaan', function () {
    $posts = DB::table('pertanyaan')->get();
    dd($posts);
    return view('items.index');
});

Route::get('/hello', function () {
    echo "This is first function";
    //return view('welcome');
});

Route::get('/master', function () {
    return view('adminlte.master');
});

Route::get('/items', function () {
    return view('items.index');
});

//Versi Laravel 8
Route::get('/home', 'App\Http\Controllers\HomeController@index');
*/

