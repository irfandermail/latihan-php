@extends('adminlte.master')

@section('content')
      
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Table Pertanyaan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Tabel Pertanyaan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Tabel Pertanyaan</h3>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
      @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
      @endif
      <a class="btn btn-primary mb-2" href="/pertanyaan/create"> Tambah Pertanyaan Baru </a>
        <table id="example1" class="table table-bordered table-striped">
          <thead>
          <tr>
            <th>#</th>
            <th>Judul</th>
            <th>Isi Pertanyaan</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diperbaharui</th>
            <th>Tools</th>
          </tr>
          </thead>
          
          <tbody>
          @forelse($posts as $key => $post)
          <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $post->judul }}</td>
            <td>{{ $post->isi }}</td>
            <td>{{ $post->tanggal_dibuat }}</td>
            <td>{{ $post->tanggal_diperbaharui }}</td>
            <td>Action</td>
          </tr>      
          @empty
          <tr><td colspan="6" align="left">-- Data tidak ada --</td></tr>    
          @endforelse
          </tbody>

          <tfoot>
          <tr>
          <th>#</th>
            <th>Judul</th>
            <th>Isi Pertanyaan</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diperbaharui</th>
            <th>Tools</th>
          </tr>
          </tfoot>
        </table>
      </div>
      <!-- /.card-body -->
    </div>

    </section>
    <!-- /.content -->
  </div>

@endsection

@push('script')
    <script src="{{ asset('/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush